<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected  $fillable = ['client'];

    public function produits(){
        return $this->belongsToMany(Produits::class,'client_produit');
    }

     
}
