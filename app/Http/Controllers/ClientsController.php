<?php

namespace App\Http\Controllers;

use App\Clients;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\Clients as ResourcesClients;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesClients::collection(Clients::orderByDesc('created_at')->get());
        /*$clients = Clients::all();
        return response()->json([
            'message' => null,
            'data' => $clients
        ],200);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $clients = new Clients();

        $clients->client = $request->client;
        $clients->save();
        $clients->produits()->attach($request->produits);
        return response()->json([
            'success' => 'client ajouter avec success',
            'data' => $clients
        ],200);

        /*if(Clients::create($request->all())){
           return response()->json([
               'success' => 'client ajouter avec success'
           ],200);
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*foreach ($clients->produits as $produit) {
            return $produit;
        }*/
         $clients=Clients::find($id);
        if(!$clients){
            return response()->json([
                'error' => 'clients inexistant'
            ],200);
        }
        try{
           return new ResourcesClients($clients);
         } catch (\Exception $e) {
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clients $clients)
    {
         $clients = new Clients();

        $clients->client = $request->client;
        $clients->save();
        $clients->produits()->attach($request->produits);

        return response()->json([
            'success' => 'client ajouter avec success',
            'data' => $clients
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clients=Clients::find($id);
        if(!$clients){
            return response()->json([
                'error' => 'client inexistant'
            ],200);
        }
        try{
            if($clients->delete()){
                return response()->json([
                   'success'=> 'client supprimer avec success'
               ]);
            }else{
              return response()->json([
                  'error' => 'Suppression du client echouée'
              ]);
           }
        } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204);

    }
}
