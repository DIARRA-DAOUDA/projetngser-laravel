<?php

namespace App\Http\Controllers;

use App\Produits;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\Produits as ResourcesProduits;

class ProduitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResourcesProduits::collection(Produits::orderByDesc('created_at')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Produits::create($request->all())){
           return response()->json([
               'success' => 'Produits ajouter avec success'
           ],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produits  $produits
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produits=Produits::find($id);
        if(!$produits){
            return response()->json([
                'error' => 'Produit inexistant'
            ],200);
        }
        try{
           return new ResourcesProduits($produits);
         } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produits  $produits
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produits $produits, $id)
    {
        $produits=Produits::find($id);
        if(!$produits){
            return response()->json([
                'error' => 'Produit inexistant'
            ],200);
        }
         try{
               if($produits->update($request->all())){
                   return response()->json([
                      'success' => 'Produits modifier avec success'
                   ],200);
               }
            } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produits  $produits
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produits=Produits::find($id);
        if(!$produits){
            return response()->json([
                'error' => 'Produit inexistant'
            ],200);
        }
        try{
            if($produits->delete()){
                return response()->json([
                   'success'=> 'Produits supprimer avec success'
               ]);
            }else{
              return response()->json([
                  'error' => 'Suppression du produits echouée'
              ]);
           }
        } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204);

    }
}
