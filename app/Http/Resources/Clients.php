<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Clients extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
           'client' => 'Nom client:' . $this->client, 
           'achats' => 'Mes achats:' . $this->achats,
           'produits'=>'produits'    . $this->produits
        ];
    }
}
