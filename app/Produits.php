<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produits extends Model
{
    protected  $fillable = ['produit', 'prix'];

    public function clients(){
        return $this->belongsToMany(Client::class,'client_produit');
    }
}
