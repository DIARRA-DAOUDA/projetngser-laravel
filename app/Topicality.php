<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topicality extends Model
{
    //model form
    protected  $fillable = ['title', 'content'];
}
